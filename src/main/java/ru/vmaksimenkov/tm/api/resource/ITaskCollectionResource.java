package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskCollectionResource {

    @NotNull
    @WebMethod
    @GetMapping
    Collection<Task> get();

    @WebMethod
    @PostMapping
    void post(@NotNull @WebParam(name = "tasks") @RequestBody List<Task> tasks);

    @WebMethod
    @PutMapping
    void put(@NotNull @WebParam(name = "tasks") @RequestBody List<Task> tasks);

    @WebMethod
    @DeleteMapping
    void delete();

}
