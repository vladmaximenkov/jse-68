package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/task")
public interface ITaskResource {

    @WebMethod
    @GetMapping("/{id}")
    Task get(@NotNull @WebParam(name = "id") @PathVariable("id") String id);

    @WebMethod
    @PostMapping
    void post(@NotNull @WebParam(name = "task") @RequestBody Task task);

    @WebMethod
    @PutMapping
    void put(@NotNull @WebParam(name = "task") @RequestBody Task task);

    @WebMethod
    @DeleteMapping("/{id}")
    void delete(@NotNull @WebParam(name = "id") @PathVariable("id") String id);

}
