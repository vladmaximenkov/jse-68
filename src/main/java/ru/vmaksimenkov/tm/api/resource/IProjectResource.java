package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/project")
public interface IProjectResource {

    @WebMethod
    @GetMapping("/{id}")
    Project get(@NotNull @WebParam(name = "id") @PathVariable("id") String id);

    @WebMethod
    @PostMapping
    void post(@NotNull @WebParam(name = "project") @RequestBody Project project);

    @WebMethod
    @PutMapping
    void put(@NotNull @WebParam(name = "project") @RequestBody Project project);

    @WebMethod
    @DeleteMapping("/{id}")
    void delete(@NotNull @WebParam(name = "id") @PathVariable("id") String id);

}
