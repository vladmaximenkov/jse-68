package ru.vmaksimenkov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.vmaksimenkov.tm.api.resource.IProjectCollectionResource;
import ru.vmaksimenkov.tm.api.resource.IProjectResource;
import ru.vmaksimenkov.tm.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.vmaksimenkov.tm.constant.Const.*;

public class ProjectClient implements IProjectResource, IProjectCollectionResource, ProjectClientFeign {

    public void post(@NotNull final List<Project> projects) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(PROJECTS, projects, void.class);
    }

    @NotNull
    public List<Project> get() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @Nullable final Project[] projects = restTemplate.getForObject(PROJECTS, Project[].class);
        if (projects == null) return new ArrayList<>();
        return Arrays.asList(projects);
    }

    public void put(@NotNull final List<Project> projects) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(PROJECTS, projects, void.class);
    }

    public void delete() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(PROJECTS);
    }

    public void post(@NotNull final Project project) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(PROJECT, project, void.class);
    }

    @Nullable
    public Project get(@NotNull final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(PROJECT+"/{id}", Project.class, id);
    }

    public void put(@NotNull final Project project) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(PROJECT, project, void.class);
    }

    public void delete(@NotNull final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(PROJECT+"/{id}", id);
    }

}
