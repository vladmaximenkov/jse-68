package ru.vmaksimenkov.tm.exception;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class RoleNotFoundException extends AbstractException {

    public RoleNotFoundException() {
        super("Error! Role not found...");
    }

}
