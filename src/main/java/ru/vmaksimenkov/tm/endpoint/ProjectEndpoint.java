package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.api.resource.IProjectResource;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping(value = "/api/project", produces = MediaType.APPLICATION_JSON_VALUE)
@WebService(endpointInterface = "ru.vmaksimenkov.tm.api.resource.IProjectResource")
public class ProjectEndpoint implements IProjectResource {

    @NotNull
    private final ProjectService service;

    @Autowired
    public ProjectEndpoint(@NotNull final ProjectService service) {
        this.service = service;
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/{id}")
    public Project get(@NotNull @WebParam(name = "id") @PathVariable("id") final String id) {
        return service.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping
    public void post(@NotNull @WebParam(name = "project") @RequestBody final Project project) {
        service.merge(project);
    }

    @Override
    @WebMethod
    @PutMapping
    public void put(@NotNull @WebParam(name = "project") @RequestBody final Project project) {
        service.merge(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/{id}")
    public void delete(@NotNull @WebParam(name = "id") @PathVariable("id") final String id) {
        service.removeById(id);
    }

}
